/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Login } from '../login';
import { TabsPage } from '../tabs/tabs';
import { CallNumber } from '@ionic-native/call-number';

@Component({
  selector: 'intro',
  templateUrl: 'index.html'
})
export class Intro {

  public controlView:number = 1;

  constructor(public navCtrl: NavController, private callNumber: CallNumber, private storage: Storage) {
    this.storage.get('login').then((val) => {
        if(val){
          this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
        }
        else {
          console.log('No hay nada');
        }
      },
      (err) => {

      });
  }

  goTo(){
    this.navCtrl.setRoot(Login, {}, {animate: true, direction: 'forward'});
  }

  llamarEmergencia(){
    this.callNumber.callNumber("+591800107003", true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }

}

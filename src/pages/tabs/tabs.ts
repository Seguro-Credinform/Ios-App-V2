import { Component } from '@angular/core';


import { Mapa2 } from '../mapa2';
import { Servicios } from '../servicios';
import { Informacion } from '../informacion';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = Servicios;
  tab2Root = Mapa2;
  tab3Root = Informacion;

  constructor() {

  }
}

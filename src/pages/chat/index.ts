/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import {Component} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {Socket} from 'ng-socket-io';
import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';
import {ChatActive} from '../chatActive';

@Component({
  selector: 'chat',
  templateUrl: 'index.html',
  styles: []
})
export class Chat {

  public list: any = [];

  constructor(public navCtrl: NavController, private socket: Socket, public toastCtrl: ToastController, private _a: Api, private storage: Storage) {

    this.storage.get('login').then(
      value => {
        this._a.getChats(value.token).subscribe(
          res => {
            console.log(res);
            this.list = res;
            if (this.list.length > 0) {
              this.navCtrl.setRoot(ChatActive, {}, {animate: true, direction: 'forward'});
            }
            else {
              this._a.postChat(value.token).subscribe(
                res => {
                  this.navCtrl.setRoot(ChatActive, {}, {animate: true, direction: 'forward'});
                },
                err => {
                  let toast = this.toastCtrl.create({
                    message: '¡Error de conexión pruebe más tarde.!',
                    duration: 3000
                  });
                  toast.present();
                }
              )
            }
          },
          err => {
            this._a.postChat(value.token).subscribe(
              res => {
                this.navCtrl.setRoot(ChatActive, {}, {animate: true, direction: 'forward'});
              },
              err => {
                let toast = this.toastCtrl.create({
                  message: '¡Error de conexión pruebe más tarde.!',
                  duration: 3000
                });
                toast.present();
              }
            )
          }
        );
      })
  }

  goTo() {
    // this.navCtrl.setRoot(Login, {}, {animate: true, direction: 'forward'});
  }


}

/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import {Component} from '@angular/core';
import {NavController, ToastController, LoadingController} from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {Storage} from '@ionic/storage';
import {Api} from '../../app/api.service';

@Component({
  selector: 'login',
  templateUrl: 'index.html',
  styles: ['.segment-ios .segment-button{border-radius:0px !important;}', '.segment-ios .segment-button.segment-activated {background-color:white; border-bottom:2px solid #004793 !important;}']
})
export class Login {
  public natJu: string = '1';
  public usuario: string = '';
  public nTelefono: string = '';

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, private storage: Storage, private _a:Api) {
    this.storage.get('login').then((val) => {
        if(val){
          this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
        }
        else {
          console.log('No hay nada');
        }
      },
      (err) => {

      });
  }

  goTo() {
    if(!this.nTelefono || !this.usuario){
      let toast = this.toastCtrl.create({
        message: '¡Por favor introduce tus datos de acceso.!',
        duration: 3000
      });
      toast.present();
      return false;
    }

    let loader = this.loadingCtrl.create({
      content: "Espere por favor...",
    });
    loader.present();


    this._a.postLogin({nTelefono: this.nTelefono, usuario: this.usuario, ambiente:"movil"}).subscribe(
      (res)=>{
        console.log(res);
        this.storage.set('login', res);
        this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
        loader.dismiss();
        let toast = this.toastCtrl.create({
          message: '¡Bienvenidos '+res.Usuario.Nombre+'.!',
          duration: 3000
        });
        toast.present();
      },
      err=>{
        console.log(err);
        let toast = this.toastCtrl.create({
          message: '¡Error en los datos suministrados.!',
          duration: 3000
        });
        toast.present();
        loader.dismiss();
      }
    );
  }


}

/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import {Component, ViewChild, ElementRef} from '@angular/core';
import {NavController, LoadingController, ToastController, AlertController} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';


import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';
import {TabsPage} from '../tabs/tabs';
import * as _ from "lodash";

declare var google: any;
declare var lat: any;
declare var long: any;


@Component({
  selector: 'reporte-sinestro-mapa',
  templateUrl: 'index.html',
  styles: ['.segment-ios .segment-button{border-radius:0px !important;}', '.segment-ios .segment-button.segment-activated {background-color:white; border-bottom:2px solid #fecf33 !important; color:#fecf33!important;}']
})
export class ReporteSiniestroMapa {

  public list: any = [];
  public controlView: string = '1';
  public latitud: any = "";
  public longitud: any = "";
  public direccion: any = "";

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  market: any;


  public siniestro: any = {};

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private _a: Api, private storage: Storage,
              private geolocation: Geolocation, public toastCtrl: ToastController, public alertCtrl: AlertController) {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
      this.storage.get('login').then(
        value => {
          this._a.getLastSiniestro(value.token).subscribe(
            res => {
              console.log(res[0]);
              this.siniestro = res[0];
              this.loadMap();
            }
          );
        }
      );
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  // Load map only after view is initialized
  ngAfterViewInit() {

  }

  loadMap() {
    let latLng = new google.maps.LatLng(this.latitud, this.longitud);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    var market = new google.maps.Marker({
      title: 'Tu ubicación',
      icon: 'assets/images/elementos credinform/miPosition.png',
      animation: 'DROP',
      position: {
        lat: this.latitud,
        lng: this.longitud
      },
      map: map
    });

    map.addListener('click', (e) => {
      lat = e.latLng.lat();
      long = e.latLng.lng();

      market.setPosition(e.latLng);

    });

  }




  public enviar() {
    const alert = this.alertCtrl.create({
      title: 'Confirmación de Solicitud',
      message: '¿Esta seguro de enviar el siguiente reporte de siniestro?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('cancel click');
          }
        },
        {
          text: 'Enviar Reporte',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "Espere por favor...",
            });
            loader.present();

            this.storage.get('login').then(
              value => {
                let data = {
                  _id: this.siniestro._id,
                  latitud: lat,
                  longitud: long,
                  direccion: this.direccion,
                  status: 2
                };

                console.log(data);
                this._a.putReporteSiniestro(data, value.token).subscribe(
                  res => {
                    const alert = this.alertCtrl.create({
                      title: '¡Su reporte a sido enviada con éxito!',
                      subTitle: 'En breve uno de nuestras operadoras se comunicara con usted.',
                      buttons: ['Continuar']
                    });
                    loader.dismiss();
                    alert.present();
                    this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
                  },
                  err => {
                    let toast = this.toastCtrl.create({
                      message: '¡Error en las comunicaciones.!',
                      duration: 3000
                    });
                    toast.present();
                    loader.dismiss();
                  }
                );
              })
          }
        }]
    })
    alert.present();
  }

  public cerrar() {
    this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
  }
}

import {Component} from '@angular/core';
import {
  ViewController,
  NavController,
  AlertController,
  ToastController,
  LoadingController,
  ActionSheetController
} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Api} from '../../app/api.service';
import {ReporteSiniestroMapa} from '../reporteSiniestroMapa';
import {TabsPage} from '../tabs/tabs';
import {Camera, CameraOptions} from '@ionic-native/camera';

@Component({
  selector: 'reportarSiniestroCamera',
  templateUrl: 'index.html'
})
export class ReportarSiniestroCamera {

  public controlStatus: number = 1;

  public siniestro: any = {};
  public base64Image: string;
  public base64ImageArray: any = [];

  constructor(private camera: Camera, public navCtrl: NavController, public toastCtrl: ToastController, public viewCtrl: ViewController, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
              private storage: Storage, private _a: Api, public actionSheetCtrl: ActionSheetController) {

    this.storage.get('login').then(
      value => {
        this._a.getLastSiniestro(value.token).subscribe(
          res => {
            console.log(res[0]);
            this.siniestro = res[0];
          }
        );
      }
    );

  }


  public options: CameraOptions = {
    quality: 50,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    targetWidth: 90,
    targetHeight: 90
  };

  public cerrar() {
    this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
  }

  public takePhoto() {

    this.camera.getPicture(this.options).then((imageData) => {
      // const alert = this.alertCtrl.create({
      //   title: '¡Foto Capturada!',
      //   subTitle: imageData,
      //   buttons: ['Continuar']
      // });
      // alert.present();
      let data = {
        _id: this.siniestro._id,
        media: imageData
      };

      let loader = this.loadingCtrl.create({
        content: "Espere por favor...",
      });
      loader.present();

      this.storage.get('login').then(
        value => {
          this._a.postReporteSiniestroImagen(data, value.token).subscribe(
            res => {
              loader.dismiss();
              this.base64ImageArray.push('data:image/jpeg;base64,' + imageData);
            },
            err => {
              console.log(err);
              let toast = this.toastCtrl.create({
                message: '¡Error en subida.!',
                duration: 3000
              });
              toast.present();
              loader.dismiss();
            }
          );
        })
    }, (err) => {
      let toast = this.toastCtrl.create({
        message: 'Error de Subida',
        duration: 3000
      });
      toast.present();
    });

  }

  public eliminar(imagen){
    const index: number = this.base64ImageArray.indexOf(imagen);
    if (index !== -1) {
      this.base64ImageArray.splice(index, 1);
    }
  }

  public segundaEtapa() {
    this.navCtrl.setRoot(ReporteSiniestroMapa, {}, {animate: true, direction: 'forward'});
  }

}

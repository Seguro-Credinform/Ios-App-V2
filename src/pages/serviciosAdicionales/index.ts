import {Component} from '@angular/core';
import {NavController, LoadingController, AlertController, ToastController} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';

import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'servicios-adicionales',
  templateUrl: 'index.html'
})
export class ServiciosAdicionales {

  public list: any = [];
  public latitud:any = "";
  public longitud:any = "";

  constructor(public navCtrl: NavController, private _a: Api, private storage: Storage, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private geolocation: Geolocation) {
    this.storage.get('login').then(
      value => {
        this._a.getServiciosAdicionales(value.token).subscribe(
          res => {
            console.log(res);
            this.list = res;
          }
        );
      }
    );

    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }


  public enviar(item) {
    const alert = this.alertCtrl.create({
      title: 'Confirmación de Solicitud',
      message: '¿Esta seguro de solicitar el servicio de '+item.nombre+'?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('cancel click');
          }
        },
        {
          text: 'Solicitar Servicio',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "Espere por favor...",
            });
            loader.present();

            this.storage.get('login').then(
              value => {
                let data = {
                  servicioAdicional:item._id,
                  latitud: this.latitud,
                  longitud: this.longitud,
                };
                this._a.postServicioAdicional(data, value.token).subscribe(
                  res => {
                    const alert = this.alertCtrl.create({
                      title: '¡Su notificación a sido enviada con éxito!',
                      subTitle: 'En breve uno de nuestras operadoras se comunicara con usted.',
                      buttons: ['Continuar']
                    });
                    loader.dismiss();
                    alert.present();
                  },
                  err => {
                    let toast = this.toastCtrl.create({
                      message: '¡Error en las comunicaciones.!',
                      duration: 3000
                    });
                    toast.present();
                    loader.dismiss();
                  }
                );
              })
          }
        }]
    })
    alert.present();
  }
}

import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

import {PreguntasFrecuentesDetalles} from '../preguntasFrecuentesDetalles';
import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'preguntas-frecuentes',
  templateUrl: 'index.html'
})
export class PreguntasFrecuentes {

  public list:any = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,  private _a:Api,  private storage: Storage) {
    this.storage.get('login').then(
      value => {
        this._a.getPreguntasFrecuentes(value.token).subscribe(
          res=>{
            console.log(res);
            this.list = res;
          }
        );
      }
    )

  }

  public verDetalles(item) {
    console.log(item);
    let modal = this.modalCtrl.create(PreguntasFrecuentesDetalles, item);
    modal.present();
  }

}

/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

import {ReportarSiniestroNew} from '../reporteSiniestroNew';
import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'chat',
  templateUrl: 'index.html',
  styles:[]
})
export class ReporteSiniestro {

  public list: any = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private _a: Api, private storage: Storage) {
    this.storage.get('login').then(
      value => {
        this._a.getSiniestro(value.token).subscribe(
          res => {
            this.list = res;
          }
        );
      }
    );
  }

  public newReporte() {
    this.navCtrl.setRoot(ReportarSiniestroNew, {}, {animate: true, direction: 'forward'});
  }

}

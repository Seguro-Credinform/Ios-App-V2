import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Api} from '../../app/api.service';

import {Sugerencias} from '../sugerencias';
import {SugerenciaDetalles} from '../sugerenciasDetalle';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'sugerencias',
  templateUrl: 'index.html'
})
export class SugerenciasIntro {

  public list:any = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, private storage: Storage, private _a:Api) {
    this.storage.get('login').then(
      value => {
        this._a.getSugerencias(value.token).subscribe(
          res=>{
            console.log(res);
            this.list = res;
          }
        );
      }
    )

    Observable.interval(1000 * 30).subscribe(x => {
      this.storage.get('login').then(
        value => {
          this._a.getSugerencias(value.token).subscribe(
            res=>{
              console.log(res);
              this.list = res;
            }
          );
        }
      )
    });
  }

  crearSugerencia() {
    let modal = this.modalCtrl.create(Sugerencias);
    modal.present();
  }

  loadSugerencias(item){
    let modal = this.modalCtrl.create(SugerenciaDetalles, item);
    modal.present();
  }

}

/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import {Component, ViewChild, ElementRef } from '@angular/core';
import {NavController, LoadingController, ToastController} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';

import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';

declare var google: any;


@Component({
  selector: 'mapa2',
  templateUrl: 'index.html',
  styles: ['.segment-ios .segment-button{border-radius:0px !important;}', '.segment-ios .segment-button.segment-activated {background-color:white; border-bottom:2px solid #fecf33 !important; color:#fecf33!important;}']
})
export class Mapa2 {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  public markers = [];

  public list: any = [];
  public controlView: string = '1';
  public latitud: number = 0;
  public longitud: number = 0;


  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, private _a: Api, private storage: Storage,  private geolocation: Geolocation, public toastCtrl: ToastController) {
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords);
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
      this.storage.get('login').then(
        value => {
          this._a.getUbicaciones(value.token).subscribe(
            res => {
              console.log(res);
              this.list = res;
              this.loadMap();
            }
          );
        }
      );
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }


  loadMap(){

    let latLng = new google.maps.LatLng(this.latitud, this.longitud);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarkets();
  }

  public addMarkets(event:any = false) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }


    let aux = 1;
    if (event) {
      aux = event.value;
    }
    else {

    }
    let miPosition = new google.maps.Marker({
      title: 'Tu ubicación',
      icon: 'assets/images/elementos credinform/miPosition.png',
      animation: 'DROP',
      position: {
        lat: this.latitud,
        lng: this.longitud
      },
      map:this.map
    });

    this.markers.push(miPosition);

    this.list.forEach(marker => {
      if (marker.tipoUbicacion == aux) {
        let oficina = new google.maps.Marker(
          {
            position: {
              lat: parseFloat(marker.latitud),
              lng: parseFloat(marker.longitud),
            },
            animation: 'DROP',
            icon: 'assets/images/elementos credinform/oficina-icon.png',
            map:this.map
          });

        let contentString = '<div>' +
          '<h2>'+'Oficina ' + marker.nombre+'</h2>' +
          '<p>' + marker.direccion+'</p>' +
          '<p>Tel: ' + marker.telefonos[0]+'</p>' +
          '<p>Horario: ' + marker.horario+'</p>' +
          '</div>';

        let htmlInfoWindow = new google.maps.InfoWindow({
          content: contentString
        });


        oficina.addListener('click', function() {
          htmlInfoWindow.open(this.map, oficina);
        });

        this.markers.push(oficina);

      }

    });
  }



}

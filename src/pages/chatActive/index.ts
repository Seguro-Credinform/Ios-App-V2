/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import {Component, ElementRef, ViewChild} from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {Socket} from 'ng-socket-io';
import {Api} from '../../app/api.service';
import {Storage} from '@ionic/storage';
import {TabsPage} from '../tabs/tabs';

@Component({
  selector: 'chat-active',
  templateUrl: 'index.html',
  styles: []
})
export class ChatActive {

  public chat: any = {};
  public listMensajes: any = [];
  public mensajeEnviar:string = '';
  @ViewChild('msjChat') private myScrollContainer: ElementRef;

  constructor(public navCtrl: NavController, private socket: Socket, public toastCtrl: ToastController, private _a: Api, private storage: Storage) {

    this.socket.on('connect', (res)=>{
      console.log('conectado');
    });

    this.storage.get('login').then(
      value => {
        this._a.getChats(value.token).subscribe(
          res => {
            console.log(res);
            this.chat = res[0];
            this.socket.emit('login',JSON.stringify({chat:this.chat._id, emisor:1}));
            this.socket.on('respuesta', (data)=>{
              console.log(data);
              this.listMensajes.push(JSON.parse(data));
              console.log(this.listMensajes);
            });
            this._a.getMensajes(this.chat._id, value.token).subscribe(
              res => {
                console.log(res);
                this.listMensajes = res;
              })
          }
        );
      })


  }

  public enviarMsj(){
    if(!this.mensajeEnviar){
      return false
    }

    this.socket.emit('mensaje', this.mensajeEnviar);
    this.mensajeEnviar = '';
  }

  public cerrar() {
    this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
  }


}

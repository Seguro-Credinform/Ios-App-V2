import { Component } from '@angular/core';
import { ViewController, NavController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Api} from '../../app/api.service';


@Component({
  selector: 'sugerencias',
  templateUrl: 'index.html',
  styles:['.segment-ios .segment-button{border-radius:0px !important;}', '.segment-ios .segment-button.segment-activated {background-color:white; border-bottom:2px solid #fecf33 !important; color:#fecf33!important;}']
})

export class Sugerencias {

  public list:any = [];
  public tipo:string = '1';
  public asunto:string = '';
  public mensaje: string = '';

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public viewCtrl: ViewController,  public alertCtrl: AlertController, public loadingCtrl: LoadingController, private storage: Storage, private _a:Api) {
  }

  public enviar(){
    if(!this.asunto || !this.mensaje){
      let toast = this.toastCtrl.create({
        message: '¡Complete el formulario antes de enviarlo.!',
        duration: 3000
      });
      toast.present();
      return false;
    }

    let loader = this.loadingCtrl.create({
      content: "Espere por favor...",
    });
    loader.present();

    this.storage.get('login').then(
      value => {
        let data = {
          tipoSugerencia:this.tipo,
          asunto:this.asunto,
          mensaje:this.mensaje
        };
        this._a.postSugerencias(data, value.token).subscribe(
          res=>{
            const alert = this.alertCtrl.create({
              title: '¡Su mensaje fue enviado con éxito.!',
              subTitle: 'Gracias por su participación.',
              buttons: ['Continuar']
            });
            loader.dismiss();
            alert.present();
            this.cerrar();
          },
          err =>{
            let toast = this.toastCtrl.create({
              message: '¡Error en los datos suministrados.!',
              duration: 3000
            });
            toast.present();
            loader.dismiss();
          }
        );
      }
    )

  }

  public cerrar() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

}

/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import { Component, ViewChild } from '@angular/core';
import { Nav, NavController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Storage } from '@ionic/storage';
import { Chat } from '../chat';
import { ChatActive } from '../chatActive';
import { Login } from '../login';
import { ReporteSiniestro } from '../reporteSiniestro';
import { ServiciosAdicionales } from '../serviciosAdicionales';


@Component({
  selector: 'servicios',
  templateUrl: 'index.html',
  styles:[]
})
export class Servicios {

  public chat = Chat;
  public reporte = ReporteSiniestro;
  public adicionales = ServiciosAdicionales;
  @ViewChild(Nav) nav: Nav;

  public controlView:number = 1;

  constructor(public navCtrl: NavController, private callNumber: CallNumber, private storage: Storage) {
  }

  llamarEmergencia(){
    this.callNumber.callNumber("+591800107003", true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }

  openPage(page) {
    this.navCtrl.push(page.component);
  }

  closeSesion(){
    this.storage.remove('login');
    this.navCtrl.setRoot(Login, {}, {animate: true, direction: 'forward'});
  }

}

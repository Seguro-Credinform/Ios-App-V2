import { Component } from '@angular/core';
import { ViewController , NavParams} from 'ionic-angular';

@Component({
  selector: 'preguntas-frecuentes-detalle',
  templateUrl: 'index.html'
})
export class PreguntasFrecuentesDetalles {

  public details:any = {
    titulo:'',
    resumen:'',
    texto:''
  };

  constructor(public params: NavParams, public viewCtrl: ViewController) {
    this.details.titulo = params.get('titulo');
    this.details.resumen = params.get('resumen');
    this.details.texto = params.get('texto');
  }

  public cerrar() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

}

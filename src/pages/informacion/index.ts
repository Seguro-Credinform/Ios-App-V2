/**
 * Created by Desarrollo KJ on 2/7/2017.
 */
import { Component, ViewChild } from '@angular/core';
import { Nav, NavController } from 'ionic-angular';

import { PreguntasFrecuentes } from '../preguntasFrecuentes';
import { SugerenciasIntro } from '../sugerenciasIntro';


@Component({
  selector: 'informacion',
  templateUrl: 'index.html',
  styles:[]
})
export class Informacion {

  preguntasFrecuentes = PreguntasFrecuentes;
  sugerencias = SugerenciasIntro;
  @ViewChild(Nav) nav: Nav;

  constructor(public navCtrl: NavController) {

  }

  openPage(page) {
    this.navCtrl.push(page.component);
  }

}

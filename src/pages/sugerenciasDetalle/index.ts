import { Component } from '@angular/core';
import { ViewController , NavParams} from 'ionic-angular';

@Component({
  selector: 'preguntas-frecuentes-detalle',
  templateUrl: 'index.html'
})
export class SugerenciaDetalles {

  public details:any = {
    asunto: "",
    mensaje: "",
    respuesta: "",
    status: 0,
    tipoSugerencia: 0
  };

  constructor(public params: NavParams, public viewCtrl: ViewController) {
    this.details.asunto = params.get('asunto');
    this.details.mensaje = params.get('mensaje');
    this.details.respuesta = params.get('respuesta');
    this.details.status = params.get('status');
    this.details.tipoSugerencia = params.get('tipoSugerencia');
  }

  public cerrar() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

}

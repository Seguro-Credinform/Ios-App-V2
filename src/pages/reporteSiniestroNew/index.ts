import {Component} from '@angular/core';
import {
  ViewController,
  NavController,
  AlertController,
  ToastController,
  LoadingController,
  ActionSheetController
} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {Storage} from '@ionic/storage';
import {Api} from '../../app/api.service';
import {TabsPage} from '../tabs/tabs';
import {ReportarSiniestroCamera} from '../reporteSiniestroCamare';
import {ReporteSiniestroMapa} from '../reporteSiniestroMapa';

@Component({
  selector: 'reportarSiniestroNew',
  templateUrl: 'index.html'
})
export class ReportarSiniestroNew {

  public controlStatus: number = 1;


  public tipo: string = '';
  public fecha: string = '';
  public descripcion: string = '';
  public latitud: any = "";
  public longitud: any = "";

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public viewCtrl: ViewController, public alertCtrl: AlertController, public loadingCtrl: LoadingController,
              private storage: Storage, private _a: Api, public actionSheetCtrl: ActionSheetController, private geolocation: Geolocation) {


    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud = resp.coords.longitude;
      this.storage.get('login').then(
        value => {
        }
      );
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  public cerrar() {
    this.navCtrl.setRoot(TabsPage, {}, {animate: true, direction: 'forward'});
  }

  public primeraEtapa() {
    if (!this.tipo || !this.descripcion || !this.fecha) {
      let toast = this.toastCtrl.create({
        message: '¡Complete el formulario antes de enviarlo.!',
        duration: 3000
      });
      toast.present();
      return false;
    }

    let data = {
      tipoSiniestro: this.tipo,
      fechaAccidente: this.fecha,
      descripcion: this.descripcion
    }

    let loader = this.loadingCtrl.create({
      content: "Espere por favor...",
    });
    loader.present();

    this.storage.get('login').then(
      value => {
        this._a.postSiniestro(data, value.token).subscribe(
          res => {
            loader.dismiss();
            console.log(res);
            this.navCtrl.setRoot(ReportarSiniestroCamera, {}, {animate: true, direction: 'forward'});
          },
          err => {
            let toast = this.toastCtrl.create({
              message: '¡Error en los datos suministrados.!',
              duration: 3000
            });
            toast.present();
            loader.dismiss();
          }
        );
      })

  }

}

import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from "@angular/http";
import { MyApp } from './app.component';


import { TabsPage } from '../pages/tabs/tabs';
import { Intro } from '../pages/intro';
import { Login } from '../pages/login';
import { Servicios } from '../pages/servicios';
import { Informacion } from '../pages/informacion';
import { Mapa2 } from '../pages/mapa2';
import { Chat } from '../pages/chat';
import { ChatActive } from '../pages/chatActive';
import { PreguntasFrecuentes } from '../pages/preguntasFrecuentes';
import { PreguntasFrecuentesDetalles } from '../pages/preguntasFrecuentesDetalles';
import { Sugerencias } from '../pages/sugerencias';
import { SugerenciasIntro } from '../pages/sugerenciasIntro';
import { SugerenciaDetalles } from '../pages/sugerenciasDetalle';
import { ReporteSiniestro } from '../pages/reporteSiniestro';
import { ReportarSiniestroNew } from '../pages/reporteSiniestroNew';
import { ReporteSiniestroMapa } from '../pages/reporteSiniestroMapa';
import { ReportarSiniestroCamera } from '../pages/reporteSiniestroCamare';
import { ServiciosAdicionales } from '../pages/serviciosAdicionales';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CallNumber } from '@ionic-native/call-number';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
const config: SocketIoConfig = { url: 'http://admincrd.com:8000', options: {} };

import { Api } from './api.service';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    Intro,
    Login,
    Servicios,
    Informacion,
    Mapa2,
    Chat,
    ChatActive,
    PreguntasFrecuentes,
    PreguntasFrecuentesDetalles,
    Sugerencias,
    SugerenciasIntro,
    SugerenciaDetalles,
    ReporteSiniestro,
    ReportarSiniestroNew,
    ReporteSiniestroMapa,
    ReportarSiniestroCamera,
    ServiciosAdicionales
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    Intro,
    Login,
    Servicios,
    Mapa2,
    Chat,
    ChatActive,
    PreguntasFrecuentes,
    PreguntasFrecuentesDetalles,
    Sugerencias,
    SugerenciasIntro,
    SugerenciaDetalles,
    ReporteSiniestro,
    ReportarSiniestroNew,
    ReporteSiniestroMapa,
    ReportarSiniestroCamera,
    ServiciosAdicionales
  ],
  providers: [
    Api,
    StatusBar,
    SplashScreen,
    CallNumber,
    Geolocation,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

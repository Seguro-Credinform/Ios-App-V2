import {Injectable} from "@angular/core";
import {RequestOptions, Headers, Http} from "@angular/http";
import "rxjs/add/operator/map";
import * as _ from "lodash";


@Injectable()
export class Api {
  public Server: string = 'http://admincrd.com:8445/';
  public ApiUrl: string = 'v1/front/';
  public ServerWithApiUrl: string = this.Server + this.ApiUrl;


  constructor(private _http: Http) {

  }

  public postLogin(user: any): any {
    let header: Headers;
    header = new Headers();
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.post(this.ServerWithApiUrl + 'login/', user, resquestOption).map(res => res.json());
  }


  public getPreguntasFrecuentes(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'preguntasFrecuentes/?status=1', resquestOption).map(res => res.json());
  }

  public getServiciosAdicionales(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'serviciosAdicionales/?status=1', resquestOption).map(res => res.json());
  }

  public postServicioAdicional(data, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.post(this.ServerWithApiUrl + 'serviciosAdicionalesSolicitudes/', data, resquestOption).map(res => res.json());
  }


  public postSugerencias(data, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.post(this.ServerWithApiUrl + 'sugerencias/', data, resquestOption).map(res => res.json());
  }

  public postSiniestro(data, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.post(this.ServerWithApiUrl + 'siniestros/', data, resquestOption).map(res => res.json());
  }


  public getSugerencias(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'sugerencias/?sort=fechaCreacion:-1', resquestOption).map(res => res.json());
  }

  public getUbicaciones(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'ubicaciones/', resquestOption).map(res => res.json());
  }


  public getChats(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'chats/?status=1', resquestOption).map(res => res.json());
  }

  public getMensajes(idChat, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'chats/'+idChat+'/mensajes', resquestOption).map(res => res.json());
  }

  public postChat(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.post(this.ServerWithApiUrl + 'chats/', {}, resquestOption).map(res => res.json());
  }

  public getLastSiniestro(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'siniestros/?status=1&perPage=1&sort=fechaCreacion:-1', resquestOption).map(res => res.json());
  }

  public postReporteSiniestroImagen(data, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});

    let dataCopy = _.cloneDeep(data);
    let id = data._id;
    delete dataCopy._id;

    return this._http.post(this.ServerWithApiUrl + 'siniestros/imagen/post/'+id, dataCopy, resquestOption).map(res => res.json());
  }

  public deleteReporteSiniestroImagen(id, idImagen, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});


    return this._http.delete(this.ServerWithApiUrl + 'siniestros/'+id+'/imagen/'+idImagen, resquestOption).map(res => res.json());
  }

  public putReporteSiniestro(data, token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});

    let dataCopy = _.cloneDeep(data);
    let id = data._id;
    delete dataCopy._id;

    return this._http.put(this.ServerWithApiUrl + 'siniestros/'+id, dataCopy, resquestOption).map(res => res.json());
  }

  public getSiniestro(token){
    let header: Headers;
    header = new Headers();
    header.append('x-access-token', token);
    header.append('x-access-llave', '123456789');
    let resquestOption:RequestOptions = new RequestOptions({headers: header});
    return this._http.get(this.ServerWithApiUrl + 'siniestros/?status=2&sort=fechaCreacion:-1', resquestOption).map(res => res.json());
  }

}
